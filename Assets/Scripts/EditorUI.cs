using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace EditorTools
{
    public class PathDragLabel
    {
        private static List<bool> dragIns = new List<bool>();
        static public void Begin()
        {
            dragIns.Clear();
        }
        static public void End()
        {
            DragAndDrop.visualMode = dragIns.Find(o => o == true) ? DragAndDropVisualMode.Generic : DragAndDropVisualMode.None;
        }

        private bool inRect = false;
        public string Label(string label, string exlabel, int height)
        {
            Rect dropArea = EditorGUILayout.GetControlRect(new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(height) });
            GUI.Box(dropArea, label + exlabel, EditorStyles.textField);
            if (Event.current.type == EventType.DragUpdated)
            {
                Event currentEvent = Event.current;
                inRect = dropArea.Contains(currentEvent.mousePosition);
            }

            if (Event.current.type == EventType.DragExited && inRect)
            {
                return DragAndDrop.paths[0];
            }
            dragIns.Add(inRect);
            return label;
        }
    }
}
