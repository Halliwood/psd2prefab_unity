using UnityEngine;

namespace TRobot
{
    public class PrefabLayout
    {   
        public static void SetStrechStrech(RectTransform rt, float left, float top, float right, float bottom)
        {
            rt.pivot = new Vector2(0.5f, 0.5f);
            rt.anchorMin = Vector2.zero;
            rt.anchorMax = Vector2.one;
            rt.anchoredPosition= new Vector3(left, top);
            rt.sizeDelta = new Vector2(right, bottom);
        }
    }
}