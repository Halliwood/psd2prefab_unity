# psd2prefab_unity

#### 介绍
用于将psd转化成unity预制体(UGUI)。本工具是C#部分，用于生成Unity预制体。在此之前，需先使用[psd2prefab](https://gitee.com/Halliwood/psd2prefab)工具生成预制体描述文件。


#### 安装教程

下载本项目代码后，将`Assets/Script`文件夹下的脚本复制到Unity项目下。
本工具依赖于[Newtonsoft.Json.dll](https://www.newtonsoft.com/json)，请自行安装。

#### 使用说明

1.  首先使用[psd2prefab](https://gitee.com/Halliwood/psd2prefab)生成预制体描述文件。
2.  执行Unity菜单栏上的`psd2prefab/生成预制体`，将上一步生成的Json文件拖入输入框中，点击一键生成按钮。
