# psd2prefab_unity

#### Description
A tool to convert psd to a unity prefab. This is the unity C# script part. Before use this, you should use [psd2prefab](https://gitee.com/Halliwood/psd2prefab) to generate the prefab discription file first.

#### Installation

Copy the `Assets/Script` files to your unity project.
This tool depends on the [Newtonsoft.Json.dll](https://www.newtonsoft.com/json), please install it by your self.

#### Instructions

1.  Use [psd2prefab](https://gitee.com/Halliwood/psd2prefab) to generate the prefab discription file first.
2.  Execute the command `psd2prefab/生成预制体` of the unity menu, drag the json file generated at the above step and click the very button.
